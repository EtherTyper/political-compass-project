import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

# Map categorical answers to numerical scale.
response_mapping = {'Disagree': -2, 'Partially disagree': -1, 'Neutral': 0, 'Partially agree': 1, 'Agree': 2}

# Read in spreadsheet and convert answers to numbers.
df = pd.read_csv('Political Survey (Responses) - Form Responses 1.csv')
df = df.applymap(lambda s : response_mapping[s] if s in response_mapping else s)

# Split questions into sections.
classification_questions = df.iloc[:,2:5]
stance_questions = df.iloc[:,5:]

# Run PCA and identify new axes.
n_components = 4
pca = PCA(n_components=n_components)
new_scores = pca.fit_transform(stance_questions)

# Compare new axis positions with self-assessed political leanings.
classifiers = pd.concat([classification_questions, pd.DataFrame(new_scores)], axis = 1)
correlations = classifiers.corr().iloc[0:3, 3:3+n_components]

# Record each axis's weights and relevance.
axes = pd.DataFrame(pca.components_, columns = stance_questions.columns)
axes['Explained Variance'] = pca.explained_variance_ratio_
axes['Shift'] = pca.components_.dot(pca.mean_)

# Print and save output.
print(axes[['Explained Variance', 'Shift']])
print(correlations)

axes.to_csv('axes.csv')
correlations.to_csv('correlations.csv')